# QSI Styleguide

## Javascript ##

### Pre-requisites ###

* node.js
* eslint

### Base Installation ###

1. ```sudo npm install -g eslint```
2. ```sudo npm install -g eslint-config-airbnb```
3. ```sudo npm install -g eslint-plugin-import```

### Additional Packages for Frontend Development if using react.js

1. ```sudo npm install -g eslint-plugin-jsx-a11y```
2. ```sudo npm install -g eslint-plugin-react```

### Enabling for a project ###
```eslint --init```

1. Select use a popular style guide
2. Airbnb
3. React can be either yes or no
4. Config file should be JSON -- This doesn't really matter, but the instructions here are for json only



At this point, most code editors WebStorm, VSCode, Atom, Sublime Text should have plugins that support your eslint configuration. To run from the commandline, run the following command

```eslint ./**/*.js```

### Customization ###

When you initialize eslint, there will be a .eslintrc.json file created your directory. This file will extend the base style. For instance, most standard style guides will disallow console.log for javascript. However, during the process of development, one may want to enable it. Therefore, the rule can be extended as: 

```
{
    "extends": "airbnb-base",
    "rules": {
        "no-console": "off"
    }

}
```

What this does, is disables a specific rule. More extensive rules can found at: https://eslint.org/docs/user-guide/configuring






